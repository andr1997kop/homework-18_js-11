const btnEnterPassVis = document.querySelector(`#btn-enter-vis`);
const btnEnterPassUnvis = document.querySelector(`#btn-enter-unvis`);
const btnConfirmPassVis = document.querySelector(`#btn-confirm-vis`);
const btnConfirmPassUnvis = document.querySelector(`#btn-confirm-unvis`);
const inputEnter = document.querySelector(`#input-enter`);
const inputConfirm = document.querySelector(`#input-confirm`);
const btnSubmit = document.querySelector(`.btn`)
const invalidPass = document.querySelector(`.invalid-pass`)

btnEnterPassVis.addEventListener(`click`, function(event){
    btnEnterPassVis.style.display = `none`;
    btnEnterPassUnvis.style.display = `inline`;
    inputEnter.setAttribute(`type`, `text`);
});
btnEnterPassUnvis.addEventListener(`click`, function(event){
    btnEnterPassVis.style.display = `inline`;
    btnEnterPassUnvis.style.display = `none`;
    inputEnter.setAttribute(`type`, `password`);
});
btnConfirmPassVis.addEventListener(`click`, function(event){
    btnConfirmPassVis.style.display = `none`;
    btnConfirmPassUnvis.style.display = `inline`;
    inputConfirm.setAttribute(`type`, `text`);
});
btnConfirmPassUnvis.addEventListener(`click`, function(event){
    btnConfirmPassVis.style.display = `inline`;
    btnConfirmPassUnvis.style.display = `none`;
    inputConfirm.setAttribute(`type`, `password`);
});
btnSubmit.addEventListener(`click`, function (event) {
    
    if (inputEnter.value == inputConfirm.value) {
        alert(`You're welcome`);  
    }
    else{
        invalidPass.style.display = `inline`;
    };
})
